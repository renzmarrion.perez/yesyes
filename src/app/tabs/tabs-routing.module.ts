import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'current-weather',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../current-weather-tab/current-weather-tab.module').then(m => m.CurrentWeatherTabModule)
          }
        ]
      },
      {
        path: 'forecast',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../forecast-tab/forecast-tab.module').then(m => m.ForecastTabPageModule)
          }
        ]
      },
      {
        path: 'about',
        children: [
          {
            path: '',
            loadChildren: () => import('../about/about.module').then( m => m.AboutPageModule)
          }
        ]
      },
      {
        path: 'activity',
        children: [
          {
            path: '',
            loadChildren: () => import('../activity/activity.module').then( m => m.ActivityPageModule)
          }
        ]
      },
      {
        path: 'add-new-task',
        children: [
          {
            path: '',
            loadChildren: () => import('../add-new-task/add-new-task.module').then( m => m.AddNewTaskPageModule)
          }
        ]
      },
      
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
